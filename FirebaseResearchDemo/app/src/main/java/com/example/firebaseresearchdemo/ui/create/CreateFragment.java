package com.example.firebaseresearchdemo.ui.create;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.firebaseresearchdemo.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
/*
Author: Brody Stewart
Since: 2021-03-01
Fragment used to create new students in FireStore
 */
public class CreateFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private final String TAG = "firebaseResearch:";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_create, container, false);

        //Initialize FireStore Connection
        FirebaseFirestore fireStoreDB = FirebaseFirestore.getInstance();

        //Fragment Elements
        Button btnCreateRecord = root.findViewById(R.id.createRecordButton);
        EditText txtFirstName = root.findViewById(R.id.txtFirstName);
        EditText txtLastName = root.findViewById(R.id.txtLastName);
        EditText txtEmail = root.findViewById(R.id.txtEmail);

        //Create OnClick Listener
        btnCreateRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Create Map and fill with values from fragment
                Map<String, Object> student = new HashMap<>();
                student.put("firstName", txtFirstName.getText().toString());
                student.put("lastName", txtLastName.getText().toString());
                student.put("email", txtEmail.getText().toString());

                // Add the new student to the collection via the Firebase Connection
                fireStoreDB.collection("students")
                        .add(student)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                            }
                        });
            }
        });
        return root;
    }
}