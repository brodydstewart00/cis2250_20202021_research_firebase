package com.example.firebaseresearchdemo.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.firebaseresearchdemo.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
/*
Author: Brody Stewart
Since: 2021-03-01
Fragment which holds a Recyclerview used to display students from FireStore.
 */
public class ViewFragment extends Fragment {

    private RecyclerView fireStoreRecyclerView;
    private FirebaseFirestore fireStore;
    private FirestoreRecyclerAdapter viewAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_view, container, false);
        fireStoreRecyclerView = root.findViewById(R.id.recyclerView);

        //Create an instance of FireStore
        fireStore = FirebaseFirestore.getInstance();

        //Create the query for the Students collection
        Query query = fireStore.collection("students");

        //Set the query in the recyclerView, cast the data to a Student Object.
        FirestoreRecyclerOptions<Student> options = new FirestoreRecyclerOptions.Builder<Student>().setQuery(query, Student.class).build();

        viewAdapter = new FirestoreRecyclerAdapter<Student, StudentViewHolder>(options) {
            @NonNull
            @Override
            public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_row_item, parent, false);
                return new StudentViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull StudentViewHolder holder, int position, @NonNull Student model) {
                holder.txtStudentName.setText(model.getFirstName()+" "+model.getLastName());
                holder.txtStudentEmail.setText(model.getEmail());
            }
        };

        fireStoreRecyclerView.setHasFixedSize(true);
        fireStoreRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        fireStoreRecyclerView.setAdapter(viewAdapter);

        return root;
    }

    /*
    Author: Brody Stewart
    Since: 2021-03-01
    Custom ViewHolder for the Student Object
     */
    private class StudentViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtStudentName;
        private final TextView txtStudentEmail;

        public StudentViewHolder(@NonNull View itemView) {
            super(itemView);
            txtStudentName = (TextView) itemView.findViewById(R.id.txtStudentFirstName);
            txtStudentEmail = (TextView) itemView.findViewById(R.id.txtStudentEmail);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        viewAdapter.stopListening();
    }

    public void onStart() {
        super.onStart();
        viewAdapter.startListening();
    }
}